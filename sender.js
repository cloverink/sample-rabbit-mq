
var amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', function (error0, connection) {
  if (error0) {
    throw error0;
  }
  connection.createChannel(function (error1, channel) {
    if (error1) {
      throw error1;
    }

    var queue = 'hello';


    channel.assertQueue(queue, {
      durable: false
    });

    for(i=1;i<=10000;i++) {
      var msg = data = [{
        id: i
      }];
      channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
    }


    console.log(" [x] Sent %s", msg);
  });


  setTimeout(function () {
    connection.close();
  }, 10);
});